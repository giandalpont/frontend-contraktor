// Application title
export const APP_NAME = 'Frontend Contraktor';

// Database connection error
export const CONN_ERROR_MSG = 'Erro de conexão';

// Enable data mock for tests
export const USE_MOCK = true;

// Default file paths
// TODO link API
export const BASE_URL = 'http://frontendcontraktor.rf.gd/api';
// TODO link download
export const DOWNLOAD_URL = 'http://frontendcontraktor.rf.gd/contraktor';
