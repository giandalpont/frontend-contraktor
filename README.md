# Frontend Contraktor
Desenvolvimento um CRUD em React.js

Deply: (http://frontendcontraktor.rf.gd)

## Download

Clone o repositório com o comando "git clone https://gitlab.com/giandalpont/frontend-contraktor.git"
depois abra o diretório "frontend-contraktor/app" e execute os comandos

## Instalação

```
yarn install - instala as dependências
yarn start - executa o projeto
```
## Introdução
### Contratos /  Partes
    É aonde fica todos os Contratos/Partes registrados.
    
![Cadastros](/image/contratos.png)

### Adicionar Contrato/Parte
    Clicando no mais, poderá adicionar novo contrato/parte.
    Campos Cadastro: Título, Data de Ativação, Data de Vencimento e anexando o aquivo.
    Campos Partes: Nome, Sobrenome, e-mail, Telefone, CPF,
    Já pode adicionar uma Parte/Contrato, se tiver algum registrados.
    Clicando em salvar contrato ira "salvar no banco".
![Adicionar contrato](/image/adicionar-contrato.png)


### Autor
Gian Carlos Dal Pont
(giandalpont@gmail.com)
